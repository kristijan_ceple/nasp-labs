#include <iostream>
#include <array>
#include <vector>
#include <algorithm>
#include <chrono>
#include <unordered_map>
#include <map>
#include <list>
#include <unordered_set>

class CP_sudoku {
public:
    struct SudokuField {
        int field[9][9];

        friend std::ostream& operator<<(std::ostream& os, const CP_sudoku::SudokuField& sf) {
            for(int i = 0; i < 9; i++) {
                for(int j = 0; j < 9; j++) {
                    std::cout << sf.field[i][j] << ", ";
                }
                std::cout << std::endl;
            }

            return os;
        }
    };

    template <size_t rows, size_t cols>
    SudokuField *solve(int (&init_values)[rows][cols]);

    int getBacktraceCalls();

    static bool isGreaterThan1(int number);
private:
    static inline constexpr int FIXED_VALUE = 0;

    //std::priority_queue<std::array<int,2>, std::vector<std::array<int,2>>, SudokuDomain> keysPrioQ;
    struct BiMap {
        // Adapted from answer by fredoverflow from: https://stackoverflow.com/questions/8026890/c-how-to-insert-array-into-hash-set
        struct ArrayHash {
            std::size_t operator()(std::array<int,2> const& v) const {
                std::hash<int> hasher;
                size_t seed = hasher(v[0]);
                seed = seed * 31 + hasher(v[1]);
                return seed;
            }
        };

        struct DomainComp {
            bool operator()(const std::unordered_set<int>* first, const std::unordered_set<int>* second) const {
                return first->size() < second->size();
            }
        };

        std::unordered_multimap<std::array<int,2>, std::unordered_set<int>, ArrayHash> allPositionDomains{};
        std::multimap<std::unordered_set<int>*, const std::array<int,2>*, DomainComp> toFillDomains{};
    };
    BiMap sudokuDomains;
    void updateDomains(int row, int col, int candidate);

    int backtraceCalls = 0;
    SudokuField* mySudokuSol = nullptr;
    std::map<std::array<int,2>, int> fixedValPositions;

    void initDomains();
    bool checkIfInitIsValid();

    void initFixedValPositions();
    void putBackFixedValues();

    bool solvingAlgorithm(int row, int col);
    bool constraintCheck(int row, int column, int candidate);

    std::array<int, 2> findNextFreePositionBruteForce(int currRow, int currCol);
    const std::array<int, 2>& findNextFreePositionHeuristic();
    std::unordered_multimap<const std::array<int,2>&, std::unordered_set<int>, CP_sudoku::BiMap::ArrayHash> backupCurrentDomain(int row, int col);
    void restoreBackup(int row, int col, std::unordered_multimap<const std::array<int,2>&, std::unordered_set<int>, CP_sudoku::BiMap::ArrayHash>& backupDomains);
};

int CP_sudoku::getBacktraceCalls() {
    return this->backtraceCalls;
}

template<size_t rows, size_t cols>
CP_sudoku::SudokuField *CP_sudoku::solve(int (&init_values)[rows][cols]) {
    this->mySudokuSol = new SudokuField;
    for(int i = 0; i < 9; i++) {
        for(int j = 0; j < 9; j++) {
            this->mySudokuSol->field[i][j] = init_values[i][j];
        }
    }

    if(!this->checkIfInitIsValid()) {
        return NULL;
    }

    this->initDomains();

    const auto& nextPos = this->findNextFreePositionHeuristic();
    bool res = this->solvingAlgorithm(nextPos[0], nextPos[1]);

    return res ? this->mySudokuSol : NULL;
}

void CP_sudoku::initDomains() {
    // First initialise them
    for(int i = 0; i < 9; i++) {
        for(int j = 0; j < 9; j++) {
            if(this->mySudokuSol->field[i][j] == -1) {
                // Not fixed value
                auto it = this->sudokuDomains.allPositionDomains.emplace(std::array<int,2>{i, j}, std::unordered_set<int>{1, 2, 3, 4, 5, 6, 7, 8, 9});
                this->sudokuDomains.toFillDomains.emplace(&it->second, &it->first);
//                this->sudokuDomains.toFillDomains.emplace(std::unordered_set<int>{1, 2, 3, 4, 5, 6, 7, 8, 9}, std::array<int,2>{i, j});
            }
        }
    }

    // Remove fixed values from domains
    for(int i = 0; i < 9; i++) {
        for(int j = 0; j < 9; j++) {
            int number = this->mySudokuSol->field[i][j];
            if(number != -1) {
                // Fixed value
                this->updateDomains(i, j, number);
            }
        }
    }

    // Add last element that has the longest domain!
    auto it = this->sudokuDomains.allPositionDomains.emplace(std::array<int,2>{9, 9}, std::unordered_set<int>{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
    this->sudokuDomains.toFillDomains.emplace(&it->second, &it->first);
}

/**
 * Naive brute-force backtracking implemented first.
 * CSP with heuristic second.
 *
 * @param row current row in the recursion
 * @param col current column in the recursion
 * @return false if need to backtrack, true if success
 */
bool CP_sudoku::solvingAlgorithm(int row, int col) {
    // Check base case!
    if(row == 9) {      // {9, 9} returned
        return true;
    }

    // Count the call!
    this->backtraceCalls++;

    // Else proceed to the next element! Need to check which number from our domain satisfies the constraints
    auto it = this->sudokuDomains.allPositionDomains.find({row, col});
    const auto myDomain = it->second;
    if(myDomain.empty()) {
        this->sudokuDomains.toFillDomains.emplace(&it->second, &it->first);              // This cell yet needs to be filled after backtracking
        return false;
    }

    std::unordered_multimap<const std::array<int,2>&, std::unordered_set<int>, CP_sudoku::BiMap::ArrayHash> domainsBackup = std::move(this->backupCurrentDomain(row, col));
    for(const int candidate : myDomain) {
        this->mySudokuSol->field[row][col] = candidate;         // Place the number
        this->updateDomains(row, col, candidate);

        const auto& nextPos = this->findNextFreePositionHeuristic();
        bool success = this->solvingAlgorithm(nextPos[0], nextPos[1]);
        if(success){
            return true;
        } else {
            this->mySudokuSol->field[row][col] = -1;
            // Restore domains copy!
            this->restoreBackup(row, col, domainsBackup);

//            auto domainsBackupCopy = domainsBackup;
//            domainsBackupCopy.merge(this->sudokuDomains.allPositionDomains);
//            this->sudokuDomains.allPositionDomains.swap(domainsBackup);
        }
    }

    // No numbers succeeded! Need to backtrack
    this->sudokuDomains.toFillDomains.emplace(&it->second, &it->first);       // This cell yet needs to be filled after backtracking
    return false;
}

bool CP_sudoku::constraintCheck(int row, int col, int candidate) {
    // Check same row first
    std::array<bool, 10> appeared{false, false, false, false, false, false, false, false, false, false};
    for(int j = 0; j < 9; j++) {
        int number = this->mySudokuSol->field[row][j];
        if(number == -1) {
            continue;
        } else if(appeared[number]) {
            return false;
        } else {
            appeared[number] = true;
        }
    }

    // Then check same column
    appeared.fill(false);
    for(int i = 0; i < 9; i++) {
        int number = this->mySudokuSol->field[i][col];
        if(number == -1) {
            continue;
        } else if(appeared[number]) {
            return false;
        } else {
            appeared[number] = true;
        }
    }

    // Then check the box
    appeared.fill(false);
    int boxRow = row - row%3;
    int boxCol = col - col%3;
    for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 3; j++) {
            int number = this->mySudokuSol->field[boxRow + i][boxCol + j];
            if(number == -1) {
                continue;
            } else if(appeared[number]) {
                return false;
            } else {
                appeared[number] = true;
            }
        }
    }

    return true;
}

bool CP_sudoku::isGreaterThan1(int number) {
    return number > 1;
}

void CP_sudoku::updateDomains(int row, int col, int candidate) {
    // Update same row domains
    for(int j = 0; j < 9; j++) {
        auto it = this->sudokuDomains.allPositionDomains.find({row,j});
        if(it == this->sudokuDomains.allPositionDomains.end()) {
            continue;
        } else {
            it->second.erase(candidate);
        }
    }

    // Update same column domains
    for(int i = 0; i < 9; i++) {
        auto it = this->sudokuDomains.allPositionDomains.find({i,col});
        if(it == this->sudokuDomains.allPositionDomains.end()) {
            continue;
        } else {
            it->second.erase(candidate);
        }
    }

    // Update same box domains
    int boxRow = row - row%3;
    int boxCol = col - col%3;
    for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 3; j++) {
            auto it = this->sudokuDomains.allPositionDomains.find({boxRow+i, boxCol+j});
            if(it == this->sudokuDomains.allPositionDomains.end()) {
                continue;
            } else {
                it->second.erase(candidate);
            }
        }
    }
}

void CP_sudoku::initFixedValPositions() {
    for(int i = 0; i < 9; i++) {
        for (int j = 0; j < 9; j++) {
            int numberTmp = this->mySudokuSol->field[i][j];
            if(numberTmp != -1) {
                this->fixedValPositions.emplace(std::array<int,2>{i,j}, numberTmp);
                this->mySudokuSol->field[i][j] = 0;
            }
        }
    }

    //std::sort(this->fixedValPositions.begin(), this->fixedValPositions.end());        // No need to sort std::map which is already sorted!
}

/**
 * In base case returns {9,9}
 *
 * @param currRow
 * @param currPos
 * @return the next free position!
 */
std::array<int, 2> CP_sudoku::findNextFreePositionBruteForce(int currRow, int currCol) {
    int jStart = (currCol == -1) ? 0 : currCol + 1;
    for(int i = currRow; i < 9; i++) {
        for(int j = jStart; j < 9; j++) {
            if(this->mySudokuSol->field[i][j] == -1) {
                return {i,j};
            }
        }
        jStart = 0;
    }

    return {9, 9};
}

bool CP_sudoku::checkIfInitIsValid() {
    for(int i = 0; i < 9; i++) {
        for(int j = 0; j < 9; j++) {
            int tmpNum = this->mySudokuSol->field[i][j];
            if(tmpNum != -1) {
                if(!this->constraintCheck(i, j, tmpNum)) {
                    return false;
                }
            }
        }
    }

    return true;
}

void CP_sudoku::putBackFixedValues() {
    for(const auto& pair : this->fixedValPositions) {
        auto pos = pair.first;
        this->mySudokuSol->field[pos[0]][pos[1]] = pair.second;
    }
}

const std::array<int, 2>& CP_sudoku::findNextFreePositionHeuristic() {
    auto it = this->sudokuDomains.toFillDomains.begin();
    const std::array<int,2>& toRet = *it->second;
    this->sudokuDomains.toFillDomains.erase(it);
    return toRet;
}

std::unordered_multimap<const std::array<int,2>&, std::unordered_set<int>, CP_sudoku::BiMap::ArrayHash> CP_sudoku::backupCurrentDomain(int row, int col) {
    std::unordered_multimap<const std::array<int,2>&, std::unordered_set<int>, CP_sudoku::BiMap::ArrayHash> toRet{};

    // Remember same row domains
    for(int j = 0; j < 9; j++) {
        auto it = this->sudokuDomains.allPositionDomains.find({row,j});
        if(it == this->sudokuDomains.allPositionDomains.end()) {
            continue;
        } else {
            toRet.emplace(it->first, it->second);
        }
    }

    // Update same column domains
    for(int i = 0; i < 9; i++) {
        auto it = this->sudokuDomains.allPositionDomains.find({i,col});
        if(it == this->sudokuDomains.allPositionDomains.end()) {
            continue;
        } else {
            toRet.emplace(it->first, it->second);
        }
    }

    // Update same box domains
    int boxRow = row - row%3;
    int boxCol = col - col%3;
    for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 3; j++) {
            auto it = this->sudokuDomains.allPositionDomains.find({boxRow+i, boxCol+j});
            if(it == this->sudokuDomains.allPositionDomains.end()) {
                continue;
            } else {
                toRet.emplace(it->first, it->second);
            }
        }
    }

    return std::move(toRet);
}

void CP_sudoku::restoreBackup(
        int row, int col,
        std::unordered_multimap<const std::array<int,2>&, std::unordered_set<int>, CP_sudoku::BiMap::ArrayHash>& restoreFrom
        ) {
    for(const auto& pair : restoreFrom) {
        const auto& pos = pair.first;
        const auto& data = pair.second;

        auto it = this->sudokuDomains.allPositionDomains.find(pos);
        it->second.clear();
        it->second.insert(data.begin(), data.end());
    }
}

// #################################################            FOR EDGAR           ##########################################################################

int main() {
    CP_sudoku sudoku;
    int table5Sudoku[9][9] = {
            {-1,6,1,-1,-1,7,-1,-1,3},
            {-1,9,2,-1,-1,3,-1,-1,-1},
            {-1,-1,-1,-1,-1,-1,-1,-1,-1},
            {-1,-1,8,5,3,-1,-1,-1,-1},
            {-1,-1,-1,-1,-1,-1,5,-1,4},
            {5,-1,-1,-1,-1,8,-1,-1,-1},
            {-1,4,-1,-1,-1,-1,-1,-1,1},
            {-1,-1,-1,1,6,-1,8,-1,-1},
            {6,-1,-1,-1,-1,-1,-1,-1,-1},
    };

    int table1Sudoku[9][9] = {
            {-1,1,6,3,-1,8,4,2,-1},
            {8,4,-1,-1,-1,7,3,-1,-1},
            {3,-1,-1,-1,-1,-1,-1,-1,-1},
            {-1,6,-1,9,4,-1,8,-1,2},
            {-1,8,1,-1,3,-1,7,9,-1},
            {9,-1,3,-1,7,6,-1,4,-1},
            {-1,-1,-1,-1,-1,-1,-1,-1,3},
            {-1,-1,5,7,-1,-1,-1,6,8},
            {-1,7,8,1,-1,3,2,5,-1},
    };

    auto start = std::chrono::high_resolution_clock::now();
    auto res = sudoku.solve(table5Sudoku);
    auto end = std::chrono::high_resolution_clock::now();

    // res != NULL
    if(res != NULL) {
        std::cout << "Success!" << std::endl;
        std::chrono::duration<double> duration = end - start;
        std::cout << "Time elapsed: " << duration.count() << std::endl;
        std::cout << "Backtrace calls: " << sudoku.getBacktraceCalls() << std::endl;

        std::cout << *res << std::endl;
    } else {
        std::cout << "Fail!" << std::endl;
    }
}