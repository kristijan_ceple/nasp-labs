#include <iostream>
#include <stack>
using namespace std;

enum class Side{Left, Right};

class AVLTree;

template<typename T>
class Node {
    friend class AVLTree;
private:
    T content;
    Node* parent = nullptr;
    Node* leftChild = nullptr;
    Node* rightChild = nullptr;

    int contentDuplicatesCounter = 0;
    int balanceFactor = 0;                      // Defined as Depth(Left Child) - Depth(Right Child)
    unsigned depth = 0;                         // Root is at depth 0

    std::stack<Side>& wayUp;
    AVLTree& myTree;
    inline static const T* climbingTmp = nullptr;


    unsigned calculateDepth();
    unsigned calculateBalanceFactor();
    void intervention();
    void updatePathUp();
    void clearWayUpDataStructures();

    void leftRotation();
    void rightRotation();
public:
    explicit Node(T& content, AVLTree& myTree, std::stack<Side>& wayUp);
    explicit Node(T& content, unsigned depth, Node* parent, AVLTree& myTree, std::stack<Side>& wayUp);
    explicit Node(T& content, Node* leftChild, Node* rightChild, AVLTree& myTree, std::stack<Side>& wayUp);

    T* getContent();
    ~Node();

    bool insertElement(T toInsert, unsigned depth);

    Node* getRightChild() { return rightChild; };
    Node* getLeftChild() { return leftChild; };
};

class AVLTree {
private:
    Node<char>* root = nullptr;
    std::stack<Side> wayUp{};
    void internal_PreOrderTraversal(Node<char>* tmp, string& stringBuilder);
    void internal_PostOrderTraversal(Node<char>* tmp, string& stringBuilder);
    pair<char*, char*> internal_PreOrderTraversal(Node<char>* tmp, char *key);
public:
    AVLTree() = default;
    void InsertElement(char key);
    pair<char*,char*> getChildrenNodesValues(char *key=NULL);
    char *getRootNode();
    Node<char>* getRoot();
    string PreOrderTraversal();
    string PostOrderTraversal();
    void setRoot(Node<char>& newRoot) {root = &newRoot;};
    ~AVLTree() { delete root; };
};

template<typename T>
Node<T>::Node(T& content, AVLTree& myTree, std::stack<Side>& wayUp)
: content{content}, myTree{myTree}, wayUp{wayUp} {}

template<typename T>
Node<T>::Node(T& content, Node* leftChild, Node* rightChild, AVLTree& myTree, std::stack<Side>& wayUp)
        : content{content}, leftChild{leftChild}, rightChild{rightChild}, myTree{myTree}, wayUp{wayUp}
{}

template<typename T>
Node<T>::Node(T& content, unsigned int depth, Node* parent, AVLTree& myTree, std::stack<Side>& wayUp)
: content {content}, depth {depth}, parent {parent}, myTree{myTree}, wayUp{wayUp}
{}

template<typename T>
Node<T>::~Node()
{
    delete this->leftChild;
    delete this->rightChild;
}

template<typename T>
T* Node<T>::getContent()
{
    return &content;
}

template<typename T>
bool Node<T>::insertElement(T toInsertKey, unsigned myDepth)
{
    unsigned childDepth = myDepth + 1;
    bool additionHappened;
    // First check where should the key go!
    if(toInsertKey < this->content) {
        if(this->leftChild == nullptr) {
            this->leftChild = new Node(toInsertKey, childDepth, this, this->myTree, this->wayUp);
            this->climbingTmp = &this->leftChild->content;
            additionHappened = true;
        } else {
            additionHappened = this->leftChild->insertElement(toInsertKey, childDepth);
        }
    } else if (toInsertKey > this->content) {
        if(this->rightChild == nullptr) {
            this->rightChild = new Node(toInsertKey, childDepth, this, this->myTree, this->wayUp);
            this->climbingTmp = &this->rightChild->content;
            additionHappened = true;
        } else {
            additionHappened = this->rightChild->insertElement(toInsertKey, childDepth);
        }
    } else {
        // Increase the counter of duplicates - and that's it - we're done
        this->contentDuplicatesCounter++;
        return false;
    }


    // Check if an addition had happened
    /*
     * From the place of insertion have to go all the way up and check the balanceFactors
     */
    if (additionHappened) {
        // Okay now we're climbing up
//        if(&leftChild->content == climbingTmp) {
//            this->wayUp.emplace(Side::Left);
//        } else if(&rightChild->content == climbingTmp) {
//            this->wayUp.emplace(Side::Right);
//        }

        this->balanceFactor = this->calculateBalanceFactor();
        switch(balanceFactor) {
            case 0:
                // Stop
                this->clearWayUpDataStructures();
                return false;
            case 1:
            case -1:
                // Go on!
                this->updatePathUp();
                return true;
            case 2:
            case -2:
                // Covers 2 and -2
                this->updatePathUp();
                this->intervention();
                return false;
            default:
                throw logic_error("Balance Factor > |2|!");
        }
    } else {
        return false;
    }
}

template<typename T>
unsigned Node<T>::calculateDepth()
{
    unsigned leftChildDepth;
    unsigned rightChildDepth;

    if(this->leftChild == nullptr) {
        leftChildDepth = 0;
    } else {
        leftChildDepth = this->leftChild->calculateDepth();
    }

    if(this->rightChild == nullptr) {
        rightChildDepth = 0;
    } else {
        rightChildDepth = this->rightChild->calculateDepth();
    }

    return 1 + max(leftChildDepth, rightChildDepth);

}

template<typename T>
unsigned Node<T>::calculateBalanceFactor()
{
    unsigned leftChildDepth;
    unsigned rightChildDepth;

    if(this->leftChild != nullptr) {
        leftChildDepth = leftChild->calculateDepth();
    } else {
        leftChildDepth = 0;
    }

    if(this->rightChild != nullptr) {
        rightChildDepth = rightChild->calculateDepth();
    } else {
        rightChildDepth = 0;
    }

    return (rightChildDepth - leftChildDepth);
}

template<typename T>
void Node<T>::intervention()
{
    // Have to check the path here!
    Side lower = wayUp.top(); wayUp.pop();
    Side lowest = wayUp.top(); wayUp.pop();

    switch(lower) {
        case Side::Left:
            switch(lowest) {
                case Side::Left:
                    // Left - left
                    this->leftChild->rightRotation();
                    break;
                case Side::Right:
                    // Left - right
                    this->leftChild->rightChild->leftRotation();
                    this->leftChild->rightRotation();
                    break;
            }
            break;
        case Side::Right:
            switch(lowest) {
                case Side::Left:
                    // Right - left
                    this->rightChild->leftChild->rightRotation();
                    this->rightChild->leftRotation();
                    break;
                case Side::Right:
                    // Right - right
                    this->rightChild->leftRotation();
                    break;
            }
            break;
    }

    // Have to clear the structure after this intervention is complete!
    this->clearWayUpDataStructures();
}

template<typename T>
void Node<T>::clearWayUpDataStructures()
{
    while(!this->wayUp.empty()) {
        this->wayUp.pop();
    }
    climbingTmp = nullptr;
}

template<typename T>
void Node<T>::updatePathUp()
{
    if(this->leftChild != nullptr) {
        if(&this->leftChild->content == climbingTmp) {
            this->wayUp.emplace(Side::Left);
        }
    }

    if(this->rightChild != nullptr) {
        if(&this->rightChild->content == climbingTmp) {
            this->wayUp.emplace(Side::Right);
        }
    }

    this->climbingTmp = &this->content;
}

template<typename T>
void Node<T>::rightRotation()
{
    // Remember parent
    Node<T>* tmp = this->parent;

    // Update this node's parent
    this->parent = tmp->parent;
    if(tmp->parent != nullptr) {
        if(tmp->parent->leftChild == tmp) {
            tmp->parent->leftChild = this;
        } else {
            tmp->parent->rightChild = this;
        }
    } else {
        // Updating root!
        myTree.setRoot(*this);
    }

    // Rewire now!
    tmp->leftChild = this->rightChild;
    if(tmp->leftChild != nullptr) {
        tmp->leftChild->parent = tmp;
    }

    // Move on to rewiring
    this->rightChild = tmp;
    tmp->parent = this;
}

template<typename T>
void Node<T>::leftRotation()
{
    // Remember parent
    Node<T>* tmp = this->parent;

    // Update parents
    this->parent = tmp->parent;
    if (tmp->parent != nullptr) {
        if(tmp->parent->leftChild == tmp) {
            tmp->parent->leftChild = this;
        } else {
            tmp->parent->rightChild = this;
        }
    } else {
        // Updating root!
        myTree.setRoot(*this);
    }

    // Rewiring
    tmp->rightChild = this->leftChild;
    if(tmp->rightChild != nullptr) {
        tmp->rightChild->parent = tmp;
    }

    tmp->parent = this;
    this->leftChild = tmp;
}

char *AVLTree::getRootNode()
{
    return this->root->getContent();
}

pair<char*, char*> AVLTree::internal_PreOrderTraversal(Node<char>* tmp, char *key)
{
    if(*(tmp->getContent()) == *key) {
        // Found our Node!
        return { tmp->getLeftChild()->getContent(), tmp->getRightChild()->getContent() };
    } else {
        // Didn't find our node!
        // Search left and right!
        if(tmp->leftChild == nullptr) {
            // Can't launch search here
            // Must search right child
            if(tmp->rightChild == nullptr) {
                // Can't launch search here either, therefore not found!
                return {NULL, NULL};
            } else {
                // At least we can search the right child
                return std::move(internal_PreOrderTraversal(tmp->getRightChild(), key));
            }
        } else {
            //Okay, we can search the left child, but is the right child an option?
            if(tmp->rightChild == nullptr) {
                // Right child isn't an option - so we can only search left child
                return std::move(internal_PreOrderTraversal(tmp->getLeftChild(), key));
            } else {
                // We can search both options!
                pair<char*, char*> left = std::move(internal_PreOrderTraversal(tmp->getLeftChild(), key));
                pair<char*, char*> right = std::move(internal_PreOrderTraversal(tmp->getRightChild(), key));

                // Now check which one was better!
                if(right.first == NULL && right.second == NULL) {
                    return { left.first, left.second };
                } else if(left.first == NULL && left.second == NULL) {
                    return { right.first, right.second };
                }
            }
        }
    }
}

pair<char *, char *> AVLTree::getChildrenNodesValues(char* key)
{
    if(root == nullptr) {
        return { NULL, NULL };
    } else if(key == NULL) {
        return {root->getLeftChild()->getContent(), root->getRightChild()->getContent()};
    }

    // Go in order until we find the value
    return internal_PreOrderTraversal(root, key);
}

void AVLTree::InsertElement(char toInsertKey)
{
    if(root == nullptr) {
        root = new Node<char>(toInsertKey, *this, wayUp);
    } else {
        bool ret = root->insertElement(toInsertKey, 0);
        if(ret) {
            root->clearWayUpDataStructures();
        }
    }
}

Node<char>* AVLTree::getRoot() {
    return this->root;
}

string AVLTree::PreOrderTraversal() {
    string toRet;
    this->internal_PreOrderTraversal(root, toRet);
    return move(toRet);
}

string AVLTree::PostOrderTraversal() {
    string toRet;
    this->internal_PostOrderTraversal(root, toRet);
    return move(toRet);
}

void AVLTree::internal_PostOrderTraversal(Node<char>* tmp, string &stringBuilder) {
    // PostOrder = left, right, this
    if(tmp == nullptr || tmp->getContent() == nullptr) {
        return;
    }

    internal_PostOrderTraversal(tmp->getLeftChild(), stringBuilder);
    internal_PostOrderTraversal(tmp->getRightChild(), stringBuilder);
    stringBuilder += *(tmp->getContent());
}

void AVLTree::internal_PreOrderTraversal(Node<char>* tmp, string &stringBuilder) {
    // PreOrder = this, left, right
    if(tmp == nullptr) {
        return;
    }

    stringBuilder += *(tmp->getContent());
    internal_PreOrderTraversal(tmp->getLeftChild(), stringBuilder);
    internal_PreOrderTraversal(tmp->getRightChild(), stringBuilder);
}

template<typename T>
void AVLTreeStringRecr(const string& p, Node<T>* toStringNode, bool isLeft)
{
    cout << p;
    cout << (!isLeft ? "├──" : "└──" );
    if(toStringNode!=nullptr) {
        cout << *(toStringNode->getContent()) << endl;

        if(toStringNode->getRightChild() || toStringNode->getLeftChild()) {
            AVLTreeStringRecr(p+(!isLeft ? "│   " : "    "),toStringNode->getRightChild(),false);
            AVLTreeStringRecr(p+(!isLeft ? "│   " : "    "),toStringNode->getLeftChild(),true);
        }
    } else cout << endl;
}

template<typename T>
void AVLTreeString(AVLTree& toPrintTree)
{
    AVLTreeStringRecr<T>("", toPrintTree.getRoot(), true);
}

int main() {
    cout << "AVL Lab!" << endl;

    AVLTree avlTree;
    avlTree.InsertElement('a');
    avlTree.InsertElement('c');
    avlTree.InsertElement('h');
    avlTree.InsertElement('k');
    avlTree.InsertElement('e');
    avlTree.InsertElement('d');
    avlTree.InsertElement('o');
    avlTree.InsertElement(';');
    avlTree.InsertElement('A');

    AVLTree numbas;
    numbas.InsertElement('9');
    numbas.InsertElement('8');
    numbas.InsertElement('7');
    numbas.InsertElement('6');
    numbas.InsertElement('5');
    numbas.InsertElement('4');
    numbas.InsertElement('3');
    numbas.InsertElement('2');
    numbas.InsertElement('1');
    AVLTreeString<char>(numbas);

    AVLTree lukeTree;
    lukeTree.InsertElement('J');
    lukeTree.InsertElement('B');
    lukeTree.InsertElement('T');
    lukeTree.InsertElement('A');
    lukeTree.InsertElement('K');
    lukeTree.InsertElement('E');
    lukeTree.InsertElement('C');
    lukeTree.InsertElement('X');

    AVLTreeString<char>(lukeTree);

    cout << lukeTree.PostOrderTraversal() << endl << endl;
    cout << lukeTree.PreOrderTraversal() << endl << endl;

    AVLTree wikiTree;
    wikiTree.InsertElement('F');
    wikiTree.InsertElement('B');
    wikiTree.InsertElement('G');
    wikiTree.InsertElement('A');
    wikiTree.InsertElement('D');
    wikiTree.InsertElement('I');
    wikiTree.InsertElement('C');
    wikiTree.InsertElement('E');
    wikiTree.InsertElement('H');

    AVLTreeString<char>(wikiTree);

    cout << wikiTree.PostOrderTraversal() << endl << endl;
    cout << wikiTree.PreOrderTraversal() << endl << endl;

    AVLTree getChildrenNodesTest{};
    getChildrenNodesTest.InsertElement('M');
    getChildrenNodesTest.InsertElement('K');
    getChildrenNodesTest.InsertElement('Q');
    getChildrenNodesTest.InsertElement('A');
    getChildrenNodesTest.InsertElement('L');
    getChildrenNodesTest.InsertElement('P');

    AVLTreeString<char>(getChildrenNodesTest);
    char key = 'K';
    pair<char*, char*> tmpPair;
    tmpPair = move(getChildrenNodesTest.getChildrenNodesValues(&key));
    cout << "Debug" << endl;
    cout    << (tmpPair.first ? string(1, *tmpPair.first) : "NULL") << ","
            <<  (tmpPair.second ? string(1, *tmpPair.second) : "NULL") << endl;

    key = 'Q';
    tmpPair = move(getChildrenNodesTest.getChildrenNodesValues(&key));
    cout    << (tmpPair.first ? string(1, *tmpPair.first).c_str() : "NULL") << ","
            <<  (tmpPair.second ? string(1, *tmpPair.second).c_str() : "NULL") << endl;

    return 0;
}

// #####################################################################################################################
// #######################################            OBSOLETE            ##############################################
// #####################################################################################################################
/*
 * This used to be in the ***insertElement*** method
 */
//Node<char>* tmpNode = &root;
//
//char tmpCmpVar = *(tmp->getContent());
//
//while(tmpNode != nullptr) {
//if(toInsertKey < tmpCmpVar) {
//
//} else if () {
//
//}
//}