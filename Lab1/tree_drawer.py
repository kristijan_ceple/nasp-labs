"""
https://codegolf.stackexchange.com/questions/849/print-a-binary-tree
By Tyilo
"""

import math

def g(a,o,d,l,b):
    if l<0:
        return
    c=2*b+1
    k=2*l+1
    o[k]=' '*b
    n=d-l
    p=1 if n==0 else 3*2**n-1
    o[k-1]=p/2*' '
    i=0
    for v in a[2**l-1:2**l*2-1]:
        v=' ' if v==None else v
        o[k]+=v+' '*c
        m=' ' if v==' ' else '/' if i%2==0 else '\\'
        o[k-1]+=m+max(1,b)*' ' if i%2==0 else m+p*' '
        i+=1

    g(a,o,d,l-1,c)
def f(a):
    d=int(math.log(len(a),2))
    o=['']*(d*2+2)
    g(a,o,d,d,0)
    print('\n'.join(o[1:]))